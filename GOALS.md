# Goals

- Integrate `rmxp_translator` in the source code.
- Create tests to guarantee modifications will still work.
- Create a better CLI
