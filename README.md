# RPG Maker XP, VX, and VX Ace translator

This is a translator for RPG Maker XP, VX, and VX Ace games.

This a fork of the `rmxp_translator`, which was sourced from the [RPG Maker
Translator][rpg-maker-translator] project. Unfortunately the project does not
mention the author or the license. If anyone has any information about the
original author, please contact in order to give proper credit.

## Installation

To install in your local machine run:

```sh
bin/setup
bundle exec rake install
```

This will install the following files in your gems folder:

- `rmvx_translator` for VX 
- `rmxp_translator` for XP
- `rmvxace_translator` for VX Ace

If you operating system cannot find the binaries that's because the gems binary
path is not defined.

You should append the gems binary folder to your `$PATH`.

```
# ~/.profile
export GEM_HOME="$(gem env user_gemhome)"
export PATH="$PATH:$GEM_HOME/bin"
```

Note that it's also possible to add folders in `$PATH` with `.zshenv` or
`.bashrc`.

For usage instructions read `USAGE.md`. For now the instructions are the same
as the original `rmxp_translator` project, since nothing was changed.

## Development

### Linter and Formatter

This project uses [Rubocop][rubocop] with some changes to the standard. The
configuration can be found in `.rubocop.yml`.

Rubocop can be executed like so:

```sh
bundle exec rubocop
```

However it should be used with a text editor with language server protocol
(LSP) support, such as [Code of Completion][coc], in case of NeoVim. Paired
with LSP, Rubocop will show real time problems with the code and add format
support.

### Tests

Tests were developed with [Rspec][rspec] and can be executed like so:

```sh
bundle exec rspec
```

[SimpleCov][simplecov] handles coverage and HTML is generated when Rspec is
executed. The start page will be located at `coverage/index.html`.

### Documentation

This project has [YARD][yard] documentation.

To generate HTML files:

```sh
bundle exec yard
```

The start page will be located at `doc/index.html`.

[rpg-maker-translator]: https://github.com/RPG-Maker-Translator/RPG-Maker-Translator/tree/master/3rdParty/rmxp_translator
[coc]: https://github.com/neoclide/coc.nvim
[rubocop]: https://rubocop.org/
[yard]: https://yardoc.org/
[simplecov]: https://github.com/simplecov-ruby/simplecov
[rspec]: https://rspec.info/
