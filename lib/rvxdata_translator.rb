# frozen_string_literal: true

require_relative "rvxdata_translator/version"
require_relative "rvxdata_translator/argument_parser"

module RvxdataTranslator
  class Error < StandardError; end

  def start
    ArgumentParser.parse_args(ARGV)
  end

  module_function :start
end
