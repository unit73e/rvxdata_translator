# frozen_string_literal: true

require "thor"

module RvxdataTranslator
  # Argument parsing module for CLI
  module ArgumentParser
    # Parses given command line arguments
    class CLI < Thor
      class_option :destination,
                   type: :string,
                   aliases: "-d",
                   desc: "The destination path",
                   default: "."

      class_option :version,
                   version: :string,
                   aliases: "-v",
                   desc: "The RPG Maker version",
                   enum: %w[xp vx vxace],
                   default: "xp"

      desc "dump", "Dump translations"
      def dump
        version = options[:version]
        puts "Dump #{version}"
      end

      desc "translate", "Translate script"
      def translate
        version = options[:version]
        puts "Translate #{version}"
      end

      desc "update", "Update translations"
      def update
        version = options[:version]
        puts "Update #{version}"
      end
    end

    def parse_args(args)
      CLI.start(args)
    end

    module_function :parse_args
  end
end
