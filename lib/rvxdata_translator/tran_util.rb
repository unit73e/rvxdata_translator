# A translation error
class TranslateError < StandardError
end

# A translatable string.
class TranslatableString
  # Constructs a translatable string.
  #
  # The original and translation must be UTF-8 compatible.
  #
  # @param orig [String] the original string.
  # @param tran [String] the translation.
  # @raise [Encoding::InvalidByteSequenceError] if orig or tran contain byte sequences that are not valid in UTF-8.
  # @raise [Encoding::UndefinedConversionError] if orig or tran contain characters that are not defined in UTF-8.
  # @return [TranslatableString] returns a new instance of TranslatableString.
  def initialize(orig, tran = "")
    @original = orig.force_encoding("UTF-8")
    @translation = tran.force_encoding("UTF-8")
  end

  attr_accessor :original, :translation

  # Compares two translatable string.
  #
  # The translable string is equal if the original and translation are equal.
  #
  # @return [Boolean] true if the translable strings are equal.
  def ==(other)
    original == other.original &&
      translation == other.translation
  end

  # Converts the translatable string to JSON.
  #
  # The format of the JSON is as follows:
  #  {
  #    "json_class": "TranslatableString",
  #    "original   ": original,
  #    "translation": translation
  #  }
  #
  # @param a [Array] the arguments to pass to the `to_json` method.
  # @return [String] the JSON representation of the object.
  def to_json(*a)
    {
      "json_class" => self.class.name,
      "original   " => @original,
      "translation" => @translation
    }.to_json(*a)
  end

  # Creates a translatable string from JSON.
  #
  # The JSON must have the following format:
  #  {
  #    "original   ": original,
  #    "translation": translation
  #  }
  #
  # @param o [Hash] the JSON input.
  # @return the translatable string.
  def self.json_create(o)
    new(o["original   "], o["translation"])
  end

  # Returns the translation, if not empty, or the original otherwise.
  #
  # The original argument must be UTF-8 compatible, and must be equal to the original attribute.
  #
  # @param orig [String] the original string to be compared with the original attribute.
  # @param info [Info] the information object.
  # @raise [TranslateError] if the original argument is different from the original attribute.
  # @raise [Encoding::InvalidByteSequenceError] if orig contains byte sequences that are not valid in UTF-8.
  # @raise [Encoding::UndefinedConversionError] if orig contains characters that are not defined in UTF-8.
  # @return [String] the translation, if not empty, or the original string otherwise.
  def apply(orig, info)
    eorig = String.new(orig).force_encoding("UTF-8")
    if @original != eorig
      msg =  "Invalid translation element!\n"
      msg += "  Got translation for:\n"
      msg += "    #{@original}\n"
      msg += "  Expected:\n"
      msg += "    #{eorig}\n"
      msg += "  Translation:\n"
      msg += "    #{@translation}"
      # TODO: whatever is using this should log it with a catch
      Util::Log.show_error(msg, info)
      # TODO: should be a custom exception that takes original and translation
      raise TranslateError, msg
    end

    @translation.empty? ? orig : @translation
  end

  # Updates the original string.
  #
  # If the original string is different than the original attribute, the translation is set to empty, as it is no
  # longer valid. Otherwise the translation is kept.
  #
  # This is a mutable method, so the orig is changed, instead of returning a copy.
  #
  # @param orig [String] the new original string.
  # @param info [Info] the information object.
  # @raise [Encoding::InvalidByteSequenceError] if orig contains byte sequences that are not valid in UTF-8.
  # @raise [Encoding::UndefinedConversionError] if orig contains characters that are not defined in UTF-8.
  # @return [TranslatableString] the reference to the updated translatable string.
  def update(orig, info)
    if @original != String.new(orig).force_encoding("UTF-8")
      Util::Log.show_info("Removing translation from changed string #{@original}", info)
      @original = orig
      @original.force_encoding("UTF-8")
      @translation = ""
    end

    self
  end
end

# A translatable array of strings.
class TranslatableArray
  # Constructs a new translatable array.
  #
  # The array must contain strings compatible with UTF-8 encoding.
  #
  # @param orig [Array<String>] the original strings.
  # @param tran [Array<String>] the translation strings.
  # @raise [Encoding::InvalidByteSequenceError] if orig contains byte sequences that are not valid in UTF-8.
  # @raise [Encoding::UndefinedConversionError] if orig contains characters that are not defined in UTF-8.
  # @return [TranslatableArray] returns a new instance of TranslatableArray.
  def initialize(orig, tran = nil)
    @original = orig.map { |o| o.force_encoding("UTF-8") }
    @translation = tran.nil? ? Array.new(@original.length, "") : tran
  end

  attr_accessor :original, :translation

  # Returns true if this translatable array is equal to other, or false otherwise.
  #
  # Two translatable arrays are equal if the original and translation array strings are equal.
  #
  # @param other [TranslatableArray] the other object to compare with.
  # @return [Boolean] true if the two translatable arrays are equal, or false otherwise.
  def ==(other)
    original == other.original &&
      translation == other.translation
  end

  # Converts the translatable array to JSON.
  #
  # The format of the JSON is as follows:
  #  {
  #    "json_class": "TranslatableArray",
  #    "original   ": original,
  #    "translation": translation
  #  }
  #
  # @param a [Array] the arguments to pass to the `to_json` method.
  # @return [String] the JSON representation of the object.
  def to_json(*a)
    {
      "json_class" => self.class.name,
      "original   " => @original,
      "translation" => @translation
    }.to_json(*a)
  end

  # Creates a translatable array from JSON.
  #
  # The JSON must have the following format:
  #  {
  #    "original   ": original,
  #    "translation": translation
  #  }
  #
  # @param o [Hash] the JSON input.
  # @return the translatable array.
  def self.json_create(o)
    new(o["original   "], o["translation"])
  end

  # Returns the translation, if none of the strings is empty, or the original otherwise.
  #
  # The original argument strings must be UTF-8 compatible, and be equal to the original attribute.
  #
  # @param orig [Array<String>] the original strings to be compared with the original attribute.
  # @param info [Info] the information object.
  # @raise [TranslateError] if the original argument is different from the original attribute.
  # @raise [Encoding::InvalidByteSequenceError] if orig strings contain byte sequences that are not valid in UTF-8.
  # @raise [Encoding::UndefinedConversionError] if orig strings contain characters that are not defined in UTF-8.
  # @return [Array<String>] the translation, if none of the strings is empty, or the original otherwise.
  def apply(orig, info)
    if orig.length != @original.length
      msg = "Invalid translation element!\n"
      msg += "  Got translation for list of #{@original.length} strings."
      msg += "  Expected list of #{orig.length}."
      msg += "  Translation: #{@translation}"
      Util::Log.show_error(msg, info)
      raise TranslateError, msg
    end

    @original.zip(orig).each do |o1, o2|
      eo = String.new(o2).force_encoding("UTF-8")
      next unless o1 != eo

      msg = "Invalid translation element!\n"
      msg += "  Got translation for:\n"
      msg += "    #{o1}\n"
      msg += "  Expected:\n"
      msg += "    #{eo}\n"
      Util::Log.show_error(msg, info)
      raise TranslateError, msg
    end

    @translation.index { |t| !t.empty? }.nil? ? orig : @translation
  end

  # Updates the original strings.
  #
  # If the original argument is different than the original attribute, the translation is set to empty, as it is no
  # longer valid. Otherwise the translation is kept.
  #
  # This is a mutable method, so the object attributes are changed, instead of making a copy of the original object.
  #
  # @param orig [Array<String>] the new original strings.
  # @param info [Info] the information object.
  # @raise [Encoding::InvalidByteSequenceError] if orig strings contain byte sequences that are not valid in UTF-8.
  # @raise [Encoding::UndefinedConversionError] if orig strings have characters that are not defined in UTF-8.
  # @return [TranslatableArray] the reference to the updated translatable array.
  def update(orig, info)
    eq = orig.length == @original.length

    if eq
      @original.zip(orig).each do |o1, o2|
        if o1 != String.new(o2).force_encoding("UTF-8")
          eq = false
          break
        end
      end
    end

    unless eq
      Util::Log.show_info("Removing translation from changed string #{@original}", info)
      @original = orig.map { |o| o.force_encoding("UTF-8") }
      @translation = Array.new(@original.length, "")
    end

    self
  end
end

# Dumps an array.
#
# For each element of the array:
# - If it's a string, the string is dumped.
# - If it's an array, it's recursively called.
# - If it's another type, it is left untouched.
#
# A new array is created instead of modifying the argument.
#
# @param arr [Array] the array to be dumped.
# @return [Array] the dumped array.
def dump_array(arr)
  arr.map do |e|
    if e.instance_of?(String) then dump_string(e)
    elsif e.instance_of?(Array) then dump_array(e)
    else
      e
    end
  end
end

# Dumps a string if not empty or nil.
#
# @param str [String] the string to be dumped.
# @return [TranslatableString] the dumped string, if not empty or nil, or nil otherwise.
def dump_string(str)
  str.nil? ? nil : TranslatableString.new(str)
end

# Dumps parameters of an array.
#
# Works as follows:
# - If the array only has strings, returns a TranslatableArray of those strings.
# - Otherwise, returns the result of #dump_array.
#
# @param arr [Array] the parameters to be dumped.
# @return [TranslatableArray, Array] the dumped parameters.
# @see #dump_array
def dump_parameters(arr)
  strs = arr.select { |e| e.instance_of?(String) }
  strs.length != arr.length ? dump_array(arr) : TranslatableArray.new(strs)
end

# Utility functions for translating various data types.

# Translates a string.
#
# @param name [String] the key of the translation.
# @param orig [String] the original string.
# @param tran [Hash{String => TranslatableString}] the translations hash.
# @param info [Info] the information object.
# @return [String] the translation, if not empty or nil, or original string otherwise.
def translate_string(name, orig, tran, info)
  tran = tran[name]
  tran.nil? ? orig : tran.apply(orig, info.add(name))
end

# Translates multiple strings.
#
# For each orig element:
# - If it's a string, the string is translated.
# - If it's an array, it's recursively called.
# - If it's another type, it is left untouched.
#
# A new array is created instead of modifying the argument. The size of the new array is cut down to whichever is
# smaller, orig or trans.
#
# @param orig [Array] the original array to be translated.
# @param tran [Array] the translations.
# @param info [Info] the information object.
# @return [Array] the translated array.
def translate_strings(orig, tran, info)
  orig.zip(tran).map do |o, t|
    if o.instance_of?(String) then t.apply(o, info)
    elsif o.instance_of?(Array) then translate_strings(o, t, info)
    else
      o
    end
  end
end

# Translates an hash.
#
# @param name [String] the translation key.
# @param orig [Hash] the hash to be translated.
# @param tran [Hash] the hash with translations.
# @param info [Info] the information object.
# @return [Hash] the translated hash.
def translate_hash(name, orig, tran, info)
  tran = tran[name]

  if tran.nil?
    Util::Log.show_error("Missing attribute #{name}!", info)
    raise TranslateError, "Missing attribute #{name}!"
  end

  tran.each do |t|
    orig[t[0]].translate(t[1], info.add(name, t[0])) unless t.nil?
  end
  orig
end

# Translates a list.
#
# TODO: it's not clear what a list means here. Since there is no difference between array and list in ruby, it must
# mean something in RPGMaker.
#
# @param name [String] the translation key.
# @param orig [Array] the array to be translated.
# @param tran [Hash, Array] the translations.
# @param info [Info] the information object.
# @return [Array] the translated array.
def translate_list(name, orig, tran, info)
  info = info.add(name)
  tran = if tran.instance_of?(Hash)
           tran[name]
         else
           tran[1][name]
         end

  if tran.nil?
    Util::Log.show_error("Missing attribute #{name}!", info)
    raise TranslateError, "Missing attribute #{name}!"
  end

  orig.zip(tran).each do |o, t|
    o.translate(t, info) unless o.nil? || t.nil?
  end
  orig
end

# Translates an array.
#
# Calls #translate_strings if the translation exists and is not nil.
#
# @param name [String] the translation key.
# @param orig [Array] the array to be translated.
# @param tran [Hash] the translations.
# @param info [Info] the information object.
# @param skip_missing [Boolean] true to skip missing translations, or false otherwise.
# @raise TranslateError if skip missing is false and a translation is missing.
# @see #translate_strings
# @return [Array] the translated array.
def translate_array(name, orig, tran, info, skip_missing: false)
  info = info.add(name)
  tran = tran[name]

  if tran.nil?
    return orig if skip_missing

    Util::Log.show_error("Missing attribute #{name}!", info)
    raise TranslateError, "Missing attribute #{name}!"

  end

  translate_strings(orig, tran, info)
end

# Translate parameters.
#
# Calls #translate_strings if the translation is an Array, otherwise calls the apply method.
#
# @param name [String] the translation key.
# @param orig [Array] the array to be translated.
# @param tran [Hash] the translations.
# @param info [Info] the information object.
# @see #translate_strings
# @return [Array] the translated array.
def translate_parameters(name, orig, tran, info)
  tran = tran[name]
  return orig if tran.nil?

  if tran.instance_of?(Array)
    translate_strings(orig, tran, info.add(name))
  else
    tran.apply(orig, info)
  end
end

# Utility functions for updating various data types.

# Updates a translation.
#
# If the original string is the same, the translation is kept, otherwise
# original is replaced, and the translation is set to empty.
#
# This is a mutable object, so tran is updated.
#
# @param name [String] the translation key.
# @param orig [Array] the array to be translated.
# @param tran [Hash] the translations.
# @param info [Info] the information object.
# @return [Array] the updated translations.
def update_string(name, orig, tran, info)
  t = tran[name]

  if t.nil?
    tran[name] = if orig.nil?
                   nil
                 else
                   TranslatableString.new(orig)
                 end
    return tran
  end

  tran[name] = t.update(orig, info.add(name))
  tran
end

# Updates translations.
#
# @param orig [Array] the original strings.
# @param tran [Array] the translations.
# @param info [Info] the information object.
# @return [Array] the updated translations.
def update_strings(orig, tran, info)
  orig.zip(tran).map do |o, t|
    if o.instance_of?(String) then t.nil? ? dump_string(o) : t.update(o, info)
    elsif o.instance_of?(Array) then update_strings(o, t, info)
    else
      o
    end
  end
end

def update_hash(name, orig, tran, info)
  ut = tran[name]

  return dump_array(orig.sort) if ut.nil?

  ut = Hash[ut.map { |k, v| [k, v] }]
  orig.each do |i, o|
    t = ut[i]

    ut[i] = if t.nil?
              o
            else
              o.update(t, info.add(name, i))
            end
  end

  tran[name] = ut.sort
  tran
end

def update_list(name, orig, tran, info)
  ut = tran[name]

  return orig if ut.nil?

  ut = orig.zip(ut).map.with_index do |e, i|
    o, t = e

    if t.nil?
      o
    else
      o.update(t, info.add(name, i))
    end
  end

  tran[name] = ut
  tran
end

def update_array(name, orig, tran, info)
  t = tran[name]
  tran[name] = t.nil? ? dump_array(orig) : update_strings(orig, t, info.add(name))
  tran
end

def update_parameters(name, orig, tran, info)
  t = tran[name]

  tran[name] = if t.nil?
                 dump_array(orig)
               elsif t.instance_of?(Array)
                 update_strings(orig, t, info.add(name))
               else
                 t.update(orig, info.add(name))
               end

  tran
end
