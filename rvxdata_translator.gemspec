# frozen_string_literal: true

require_relative "lib/rvxdata_translator/version"

Gem::Specification.new do |spec|
  spec.name = "rvxdata_translator"
  spec.version = RvxdataTranslator::VERSION
  spec.authors = ["unit73e"]
  spec.email = ["unit73e@gmail.com"]

  spec.summary = "RPG Maker XP, VX, and VX Ace translator"
  spec.homepage = "https://gitlab.com/unit73e/rvxdata_translator"
  spec.required_ruby_version = ">= 2.6.0"

  # spec.metadata["allowed_push_host"] = "TODO: Set to your gem server 'https://example.com'"

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = "https://gitlab.com/unit73e/rvxdata_translator"
  spec.metadata["changelog_uri"] = "https://gitlab.com/unit73e/rvxdata_translator/-/blob/master/CHANGELOG.md"

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(__dir__) do
    `git ls-files -z`.split("\x0").reject do |f|
      (File.expand_path(f) == __FILE__) ||
        f.start_with?(*%w[bin/ test/ spec/ features/ .git .circleci appveyor Gemfile])
    end
  end
  spec.bindir = "exe"
  spec.executables = spec.files.grep(%r{\Aexe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  # Uncomment to register a new dependency of your gem
  # spec.add_dependency "example-gem", "~> 1.0"
  spec.add_dependency "json", "~> 2.6.3"
  spec.add_dependency "pathname", "~> 0.3.0"
  spec.add_dependency "zlib", "~> 3.0.0"

  # For more information and examples about making a new gem, check out our
  # guide at: https://bundler.io/guides/creating_gem.html
end
