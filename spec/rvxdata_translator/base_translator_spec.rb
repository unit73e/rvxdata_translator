# frozen_string_literal: true

require "rvxdata_translator/base_translator"

# TODO: for some strange reason these tests break everything
RSpec.describe BaseTranslator do
  subject { described_class }
  let(:translator) { subject.new }
  let(:dest) { instance_double(Pathname) }

  before do
    allow(dest).to receive(:exist?).and_return(true)
    allow(dest).to receive(:directory?).and_return(true)
    allow(Pathname).to receive(:new).and_return(dest)
  end

  describe("#set_destination") do
    it "shows error if dest does not exist" do
      allow(dest).to receive(:exist?).and_return(false)
      expect { translator.set_destination("test") }.to raise_error(
        "The given destination directory does not exist!"
      )
    end

    it "shows error if dest it not a directory" do
      allow(dest).to receive(:directory?).and_return(false)
      expect { translator.set_destination("test") }.to raise_error(
        "The given destination is not a directory!"
      )
    end

    it "sets destination if exists and is a directory" do
      expect(translator.set_destination("test")).to eq(nil)
    end
  end
end
