# frozen_string_literal: true

require "rvxdata_translator/common_db"

RSpec.describe Color do
  subject { described_class }

  let(:red) { 1.0 }
  let(:green) { 0.5 }
  let(:blue) { 0.25 }
  let(:alpha) { 0.75 }
  let(:color1) { subject.new(red, green, blue, alpha) }
  let(:color2) { subject.new(red, green, blue, alpha) }
  let(:color3) { subject.new(red, green, blue) }

  context("when initialized") do
    it "does not modify red" do
      expect(color1.red).to eq(red)
    end

    it "does not modify green" do
      expect(color1.green).to eq(green)
    end

    it "does not modify blue" do
      expect(color1.blue).to eq(blue)
    end

    it "does not modify alpha" do
      expect(color1.alpha).to eq(alpha)
    end

    it "is opaque by default" do
      expect(color3.alpha).to eq(255.0)
    end
  end

  it "equals colors with same attributes" do
    expect(color1).to eq(color2)
  end

  it "loads a dumped color" do
    expect(Marshal.load(Marshal.dump(color1))).to eq(color2)
  end
end
