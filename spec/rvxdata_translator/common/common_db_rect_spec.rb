# frozen_string_literal: true

require "rvxdata_translator/common_db"

RSpec.describe Rect do
  subject { described_class }

  let(:x) { 10 }
  let(:y) { 20 }
  let(:width) { 100 }
  let(:height) { 200 }
  let(:rect1) { subject.new(x, y, width, height) }
  let(:rect2) { subject.new(x, y, width, height) }

  context("when initialized") do
    it "does not modify x" do
      expect(rect1.x).to eq(x)
    end

    it "does not modify y" do
      expect(rect1.y).to eq(y)
    end

    it "does not modify width" do
      expect(rect1.width).to eq(width)
    end

    it "does not modify height" do
      expect(rect1.height).to eq(height)
    end
  end

  it "equals rectangles of same size and position" do
    expect(rect1).to eq(rect2)
  end

  it "loads a dumped rectangle" do
    expect(Marshal.load(Marshal.dump(rect1))).to eq(rect2)
  end
end
