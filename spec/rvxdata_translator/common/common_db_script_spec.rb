# frozen_string_literal: true

require "rvxdata_translator/common_db"

RSpec.describe Script do
  let(:index) { 20 }
  let(:name) { "myFile" }
  let(:text) { "" }
  let(:scripts_path) { Pathname.new("scripts") }
  let(:script_idx) { 3 }
  let(:script) { described_class.new(index, name, text, scripts_path, script_idx) }

  before do
    inflated = instance_double("String")
    allow(Zlib::Inflate).to receive(:inflate).with(any_args).and_return(inflated)
    allow(inflated).to receive(:force_encoding).with(any_args).and_return(inflated)
  end

  context "when initialized" do
    it "does not modify index" do
      expect(script.index).to eq(index)
    end

    it "does not modify name" do
      expect(script.name).to eq(name)
    end

    it "zips text in utf-8" do
      script
      expect(Zlib::Inflate).to have_received(:inflate).with(text)
    end

    it "joins script path with name" do
      expect(script.file).to(
        eq(Pathname.new("#{scripts_path}/Script#{script_idx.to_s.rjust(3, "0")}.rb"))
      )
    end
  end
end
