# frozen_string_literal: true

require "rvxdata_translator/common_db"

RSpec.describe Table do
  subject { described_class }

  let(:x) { 2 }
  let(:y) { 3 }
  let(:z) { 4 }
  let(:table1) { subject.new(x, y, z) }
  let(:table2) { subject.new(x, y, z) }
  let(:table3) { subject.new(x) }

  context("when initialized") do
    it "does not modify x" do
      expect(table1.xsize).to eq(x)
    end

    it "does not modify y" do
      expect(table1.ysize).to eq(y)
    end

    it "does not modify z" do
      expect(table1.zsize).to eq(z)
    end

    it "sets data to zero array" do
      expect(table1.data).to eq(Array.new(x * y * z, 0))
    end

    it "sets 1 as y default" do
      expect(table3.ysize).to eq(1)
    end

    it "sets 1 as z default" do
      expect(table3.zsize).to eq(1)
    end
  end

  it "gets data as 3d array" do
    table1[3, 2, 3] = 3
    expect(table1[3, 2, 3]).to eq(3)
  end

  it "equals tables with same attributes" do
    expect(table1).to eq(table2)
  end

  it "loads a dumped table" do
    expect(Marshal.load(Marshal.dump(table1))).to eq(table2)
  end
end
