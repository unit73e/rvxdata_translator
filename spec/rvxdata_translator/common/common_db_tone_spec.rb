# frozen_string_literal: true

require "rvxdata_translator/common_db"

RSpec.describe Tone do
  subject { described_class }

  let(:red) { 1.0 }
  let(:green) { 0.5 }
  let(:blue) { 0.25 }
  let(:gray) { 0.75 }
  let(:tone1) { subject.new(red, green, blue, gray) }
  let(:tone2) { subject.new(red, green, blue, gray) }
  let(:tone3) { subject.new(red, green, blue) }

  context("when initialized") do
    it "does not modify red" do
      expect(tone1.red).to eq(red)
    end

    it "does not modify green" do
      expect(tone1.green).to eq(green)
    end

    it "does not modify blue" do
      expect(tone1.blue).to eq(blue)
    end

    it "does not modify gray" do
      expect(tone1.gray).to eq(gray)
    end

    it "has zero gray by default" do
      expect(tone3.gray).to eq(0.0)
    end
  end

  it "equals tones with same attributes" do
    expect(tone1).to eq(tone2)
  end

  it "loads a dumped tone" do
    expect(Marshal.load(Marshal.dump(tone1))).to eq(tone2)
  end
end
