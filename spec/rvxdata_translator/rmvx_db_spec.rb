require "rvxdata_translator/rmvx_db"

RSpec.describe RPG do
  describe("get_event_type") do
    context("when code is known") do
      it "gets type and if it can be translated" do
        expect(get_event_type(108)).to eq(["Comment", true])
      end
    end

    context("when code is unknown") do
      it "gets code and can't be translated" do
        expect(get_event_type(700)).to eq([700, false])
      end
    end
  end
end

RSpec.describe RPG::Actor do
  let(:en_name) { "test" }
  let(:jp_name) { "名称" }
  let(:jp_name2) { "名前" }
  let(:actor) { sample_actor }
  let(:tran) { { "name" => TranslatableString.new(jp_name, en_name) } }
  let(:info) { Util::Info.new(TranslatableString.name, 0) }

  def sample_actor(name = jp_name)
    actor = described_class.new
    actor.name = name
    actor
  end

  describe "to_json" do
    it "creates a json string" do
      expect(actor.to_json).to eq({
        "json_class" => "RPG::Actor",
        "name" => TranslatableString.new(jp_name)
      }.to_json)
    end
  end

  describe "translate" do
    it "translates the name" do
      actor.translate(tran, info)
      expect(actor.name).to eq(en_name)
    end

    it "returns itself" do
      expect(actor.translate(tran, info)).to equal(actor)
    end
  end

  describe "update" do
    let(:actor) { sample_actor(jp_name2) }

    it "updates the name" do
      actor.update(tran, info)
      expect(tran["name"]).to eq(TranslatableString.new(jp_name2))
    end
  end
end

RSpec.describe RPG::AudioFile do
  let(:name) { "test" }
  let(:volume) { 80 }
  let(:pitch) { 70 }
  let(:file) { described_class.new(name, volume, pitch) }

  context("when initializing") do
    it "the volume and pitch are 100 by default" do
      expect(described_class.new(name)).to eq(described_class.new(name, 100, 100))
    end

    it "keeps the name intact" do
      expect(file.name).to equal(name)
    end

    it "keeps the volume intact" do
      expect(file.volume).to equal(volume)
    end

    it "keeps the pitch intact" do
      expect(file.pitch).to equal(pitch)
    end
  end
end

RSpec.describe RPG::BGM do
  let(:name) { "test" }
  let(:file) { described_class.new(name) }
  let(:audio) { instance_double("Audio") }

  before do
    stub_const("Audio", audio)
    allow(audio).to receive(:bgm_stop)
    allow(audio).to receive(:bgm_play)
    allow(audio).to receive(:bgm_fade)
  end

  shared_examples_for "stop audio" do |method|
    it "stops the audio" do
      method.call
      expect(audio).to have_received(:bgm_stop)
    end

    it "sets BGM.last to a new BGM instance" do
      method.call
      expect(described_class.last).to eq(described_class.new)
    end
  end

  describe("play") do
    context("without filename") do
      it_behaves_like "stop audio", -> { described_class.new.play }
    end

    context("with filename") do
      it "plays the audio" do
        file.play
        expect(audio).to have_received(:bgm_play).with("Audio/BGM/#{file.name}", file.volume, file.pitch)
      end

      it "sets BGM.last to the same BGM instance" do
        file.play
        expect(described_class.last).to eq(file)
      end
    end
  end

  describe("stop") do
    it_behaves_like "stop audio", -> { described_class.stop }
  end

  describe("fade") do
    let(:time) { 100 }

    it "fades the sound" do
      described_class.fade(time)
      expect(audio).to have_received(:bgm_fade).with(time)
    end

    it "sets BGM.last to a new BGM instance" do
      described_class.fade(time)
      expect(described_class.last).to eq(described_class.new)
    end
  end

  describe("last") do
    it "starts with BGM.new" do
      expect(described_class.last).to eq(described_class.new)
    end
  end
end
