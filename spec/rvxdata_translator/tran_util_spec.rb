require "rvxdata_translator/tran_util"
require "rvxdata_translator/rmvx_db"

# TODO: TranslatableString should be in its own file
RSpec.describe TranslatableString do
  context("when initialized with UTF-8") do
    let(:original) { "これはテストである" }
    let(:translation) { "This is a test" }
    let(:trans_str) { described_class.new(original, translation) }
    let(:trans_empty) { described_class.new(original, "") }

    it "does not change original" do
      expect(trans_str.original).to eq(original)
    end

    it "does not change translation" do
      expect(trans_str.translation).to eq(translation)
    end

    it "converts to json" do
      expect(trans_str.to_json).to eq({
        "json_class" => described_class.name,
        "original   " => trans_str.original,
        "translation" => trans_str.translation
      }.to_json)
    end

    it "creates from json" do
      json = { "original   " => original,
               "translation" => translation }
      expect(described_class.json_create(json)).to eq(trans_str)
    end

    describe "applies" do
      it "does not translate when original is different" do
        expect { trans_str.apply("これは", nil) }.to raise_error(TranslateError)
      end

      it "does not translate when translation is empty" do
        expect(trans_empty.apply(original, nil)).to eq(original)
      end

      it "translates when translation is not empty" do
        expect(trans_str.apply(original, nil)).to eq(translation)
      end
    end

    describe "update" do
      it "updates when original is different" do
        orig = "これは"
        expect(trans_str.update(orig, nil)).to eq(described_class.new(orig, ""))
      end

      it "does not update when original is the same" do
        expect(trans_str.update(original, nil)).to eq(trans_str)
      end
    end
  end

  context("when initialized with Shift-JIS") do
    let(:original) { "これはテストである".encode("Shift_JIS") }
    let(:translation) { "This is a test".encode("Shift_JIS") }
    let(:trans_str) { described_class.new(original, translation) }
    let(:trans_empty) { described_class.new(original, "") }

    it "forces original to UTF-8 encoding" do
      expect(trans_str.original).to eq(original.force_encoding("UTF-8"))
    end

    it "forces translate to UTF-8 encoding" do
      expect(trans_str.translation).to eq(translation.force_encoding("UTF-8"))
    end

    it "cannot be converted to json" do
      expect { trans_str.to_json }.to raise_error(
        JSON::GeneratorError,
        "source sequence is illegal/malformed utf-8"
      )
    end

    it "creates from json" do
      json = {
        "original   " => original,
        "translation" => translation
      }
      expect(described_class.json_create(json)).to eq(trans_str)
    end

    describe "applies" do
      it "does not translate when original is different" do
        expect { trans_str.apply("これは", nil) }.to raise_error(TranslateError)
      end

      it "does translate when translation is empty" do
        expect(trans_empty.apply(original, nil)).to eq(original)
      end

      it "translates when translation is not empty" do
        expect(trans_str.apply(original, nil)).to eq(translation)
      end
    end

    describe "update" do
      it "updates when original is different" do
        orig = "これは".encode("Shift_JIS")
        expect(trans_str.update(orig, nil)).to eq(described_class.new(orig, ""))
      end

      it "does not update when original is the same" do
        expect(trans_str.update(original, nil)).to eq(trans_str)
      end
    end
  end
end

RSpec.describe TranslatableArray do
  let(:original) { %w[アレイ テスト] }
  let(:translation) { %w[Array Test] }
  let(:trans_arr) { described_class.new(original, translation) }
  let(:trans_empty) { described_class.new(original, []) }

  context("when initialized with UTF-8") do
    it "does not change original" do
      expect(trans_arr.original).to eq(original)
    end

    it "does not change translation" do
      expect(trans_arr.translation).to eq(translation)
    end

    it "converts to json" do
      expect(trans_arr.to_json).to eq(
        { "json_class" => described_class.name,
          "original   " => trans_arr.original,
          "translation" => trans_arr.translation }.to_json
      )
    end

    it "creates from json" do
      json = {
        "original   " => original,
        "translation" => translation
      }
      expect(described_class.json_create(json)).to eq(trans_arr)
    end

    describe "applies" do
      it "does not translate when original size is different" do
        expect { trans_arr.apply(%w[ア テ ス], nil) }.to raise_error(TranslateError)
      end

      it "does not translate when original content is different" do
        expect { trans_arr.apply(%w[ア テ], nil) }.to raise_error(TranslateError)
      end

      it "keeps original when translation is empty" do
        expect(trans_empty.apply(original, nil)).to eq(original)
      end

      it "translates when translation is not empty" do
        expect(trans_arr.apply(original, nil)).to eq(translation)
      end
    end

    describe "updates" do
      it "updates when original is different" do
        orig = %w[アレイ 試験]
        expect(trans_arr.update(orig, nil)).to eq(described_class.new(orig, ["", ""]))
      end

      it "does not update when original is the same" do
        expect(trans_arr.update(original, nil)).to eq(trans_arr)
      end

      it "does not update when one of translation strings is empty" do
        trans = described_class.new(original, ["", "Test"])
        expect(trans.update(original, nil)).to eq(trans)
      end
    end
  end
end

RSpec.describe "dump_array" do
  it "dumps string elements" do
    expect(dump_array(["test"])).to eq([TranslatableString.new("test")])
  end

  it "recurses array elements" do
    expect(dump_array([%w[a b]])).to eq([[TranslatableString.new("a"), TranslatableString.new("b")]])
  end

  it "keeps non-string elements" do
    expect(dump_array([1])).to eq([1])
  end
end

RSpec.describe "dump_string" do
  let(:str) { "test" }
  let(:trans) { TranslatableString.new(str) }

  it "dumps string if not nil" do
    expect(dump_string(str)).to eq(trans)
  end

  it "does not dump string if nil" do
    expect(dump_string(nil)).to eq(nil)
  end
end

RSpec.describe "dump_parameters" do
  context "when all elements are strings" do
    let(:input) { %w[a b c] }

    it "creates a translatable array" do
      expect(dump_parameters(input)).to eq(TranslatableArray.new(input))
    end
  end

  context "when not all elements are strings" do
    let(:input) { [1, "c"] }

    it "dumps array" do
      expect(dump_parameters(input)).to eq(dump_array(input))
    end
  end
end

RSpec.describe "translate_string" do
  let(:en_name) { "name" }
  let(:jp_name) { "名称" }
  let(:info) { Util::Info.new(TranslatableString.name, 0) }

  it "gets translation if exists and not empty" do
    tran = { en_name => TranslatableString.new(jp_name, en_name) }
    expect(translate_string(en_name, jp_name, tran, info)).to eq(en_name)
  end

  it "gets original if translation does not exist" do
    tran = {}
    expect(translate_string(en_name, jp_name, tran, info)).to eq(jp_name)
  end

  it "gets original if translation is empty" do
    tran = { en_name => TranslatableString.new(jp_name, "") }
    expect(translate_string(en_name, jp_name, tran, info)).to eq(jp_name)
  end

  it "raises error if original is different" do
    tran = { en_name => TranslatableString.new(jp_name, en_name) }
    expect { translate_string(en_name, "名前", tran, info) }.to raise_error(TranslateError)
  end
end

RSpec.describe "translate_strings" do
  let(:en_name) { "name" }
  let(:jp_name) { "名称" }
  let(:info) { Util::Info.new(TranslatableString.name, 0) }

  it "translate string non-empty elements" do
    orig = [jp_name]
    tran = [TranslatableString.new(jp_name, en_name)]
    expect(translate_strings(orig, tran, info)).to eq([en_name])
  end

  it "recurses array elements" do
    orig = [[jp_name]]
    tran = [[TranslatableString.new(jp_name, en_name)]]
    expect(translate_strings(orig, tran, info)).to eq([[en_name]])
  end

  it "keeps non-string elements" do
    orig = [1]
    tran = [1]
    expect(translate_strings(orig, tran, info)).to eq([1])
  end
end

RSpec.describe "translate_hash" do
  let(:en_name) { "name" }
  let(:jp_name) { "名称" }
  let(:orig) { { en_name => [Class.new] } }
  let(:tran) { { en_name => [[en_name, jp_name]] } }
  let(:info) { Util::Info.new(TranslatableString.name, 0) }

  context "when translation does not exist" do
    it "raises a translate error" do
      expect { translate_hash("id", orig, tran, info) }.to raise_error(TranslateError)
    end
  end
end

RSpec.describe "update_string" do
  let(:en_name) { "name" }
  let(:jp_name) { "名称" }
  let(:orig) { en_name }
  let(:tran) { { en_name => TranslatableString.new(en_name, jp_name) } }
  let(:info) { Util::Info.new(TranslatableString.name, 0) }

  context "when tran and orig are nil" do
    let(:tran) { { en_name => nil } }
    let(:orig) { nil }

    it "updates tran[name] to nil" do
      update_string(en_name, orig, tran, info)
      expect(tran[en_name]).to eq(nil)
    end
  end

  context "when tran is nil" do
    let(:tran) { { en_name => nil } }

    it "updates tran[name] to new original with no translation" do
      update_string(en_name, orig, tran, info)
      expect(tran[en_name]).to eq(TranslatableString.new(orig))
    end
  end

  context "when orig is different from tran" do
    let(:orig) { "alias" }

    it "updates tran[name] to new original with no translation" do
      update_string(en_name, orig, tran, info)
      expect(tran[en_name]).to eq(TranslatableString.new(orig))
    end
  end

  context "when orig is equal to tran" do
    it "the tran is untouched" do
      update_string(en_name, orig, tran, info)
      expect(tran[en_name]).to eq(TranslatableString.new(en_name, jp_name))
    end
  end
end
