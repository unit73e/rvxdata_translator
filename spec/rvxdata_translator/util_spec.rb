# frozen_string_literal: true

require "rvxdata_translator/util"

RSpec.describe Util::Info do
  describe "#dump" do
    subject { described_class.new(element, index) }
    let(:element) { "one" }
    let(:index) { 1 }

    context "with one element" do
      context "with valid index" do
        it "prints element and index" do
          actual = "  from element one with index 1\n"
          expect { subject.dump }.to output(actual).to_stdout
        end
      end

      context "with invalid index" do
        let(:index) { -1 }

        actual = "  from element one\n"
        it "prints only the element" do
          expect { subject.dump }.to output(actual).to_stdout
        end
      end
    end

    context "with multiple elements" do
      it "prints all elements" do
        actual = <<-OUTPUT.gsub(/^\s+/, "  ")
          from element one with index 1
          from element two with index 2
          from element three with index 3
        OUTPUT
        expect do
          subject.add("two", 2)
                 .add("three", 3)
                 .dump
        end.to output(actual).to_stdout
      end
    end
  end
end

RSpec.describe Util::Log do
  subject { described_class }
  let(:message) { "my message" }

  shared_examples "show_message" do |method, header, message|
    context "without info" do
      it "prints the header and message" do
        actual = "#{header}: #{message}\n"
        expect { subject.send(method, message) }.to output(actual).to_stdout
      end
    end

    context "with info" do
      let(:name) { "name" }
      let(:index) { "index" }
      let(:info) { Util::Info.new(name, index) }

      it "prints the header, message and info" do
        actual = "#{header}: #{message}\n"
        actual += "  from element #{name} with index #{index}\n"
        expect { subject.send(method, message, info) }.to output(actual).to_stdout
      end
    end
  end

  it_behaves_like "show_message", :show_error, "Error"
  it_behaves_like "show_message", :show_warning, "Warning"
  it_behaves_like "show_message", :show_info, "Info"
end

RSpec.describe Util::File do
  subject { described_class }
  let(:path) { instance_double(Pathname) }
  let(:dirname) { instance_double(Pathname) }

  before do
    allow(path).to receive(:dirname).and_return(dirname)
    allow(path).to receive(:exist?).and_return(true)
    allow(path).to receive(:readable?).and_return(true)
    allow(dirname).to receive(:writable?).and_return(true)
  end

  describe "#check_readable" do
    it "shows error if path does not exist" do
      allow(path).to receive(:exist?).and_return(false)
      expect { subject.check_readable(path) }.to raise_error("#{path} does not exist!")
    end
    it "shows error if path is not readable" do
      allow(path).to receive(:readable?).and_return(false)
      expect { subject.check_readable(path) }.to raise_error("No permission to access #{path}!")
    end
    it "shows return true if readable" do
      expect(subject.check_readable(path)).to eq(nil)
    end
  end

  define "#check_writable" do
    it "shows error if directory is not writable" do
      allow(directory).to receive(:writable?).and_return(false)
      expect { subject.check_writable(path) }.to raise_error("No permission to write to #{path}!")
    end
    it "shows error if path does not exist" do
      allow(path).to receive(:exist?).and_return(false)
      expect { subject.check_writable(path) }.to raise_error("No permission to write to #{path}!")
    end
    it "shows return true if writable" do
      expect(subject.check_writable(path)).to eq(true)
    end
  end
end
